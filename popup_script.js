document.addEventListener("DOMContentLoaded", function(dcle){
    var pageInfo,
    baseUrl;

    function respSearch () {
        var resp = JSON.parse(this.responseText);
        pageInfo = resp.graphql.hashtag.edge_hashtag_to_media['page_info'];
        var count = resp.graphql.hashtag.edge_hashtag_to_media['count'],
            edges = resp.graphql.hashtag.edge_hashtag_to_media['edges'];
        
        for(obj in edges) {
            var div = document.createElement('div');
            var img = document.createElement('img');
            var node = edges[obj].node;
            img.src = node.display_url;
            img.style.width = '50px';
            div.appendChild(img);
            div.style.display = 'inline-block';
            div.style.padding = '5px';
            result.appendChild(div);
        }
    //
        window.scrollTo(0,document.body.scrollHeight);
        loadingMask.style.display = 'none';
        loadMore.style.display = pageInfo.has_next_page ? 'block' : 'none';
      }
    //
    function search(keyword, nextPage) {
        loadMore.style.display = 'none';
        previewMask.style.display = 'none';
        loadingMask.style.display = 'flex';
        keyword = encodeURI(keyword);
        baseUrl = 'https://www.instagram.com/explore/tags/'+keyword+'/?__a=1';
        if(nextPage && pageInfo) {
            baseUrl += '&max_id=' + pageInfo.end_cursor;
        } else {
            result.innerHTML = "";
        }

        var oReq = new XMLHttpRequest();
        oReq.addEventListener("load", respSearch);
        oReq.open("GET", baseUrl);
        oReq.send();
    }
    // UI
    var searchForm = document.createElement('div'),
    input = document.createElement('input'),
    result = document.createElement('div'),
    loadMore = document.createElement('div'),
    loadingMask = document.createElement('div'),
    loading = document.createElement('div'),
    previewMask = document.createElement('div'),
    preview = document.createElement('img');

    result.style.paddingTop = '40px';
    result.addEventListener('click', function(e){
        if (e.srcElement.nodeName === "IMG") {
            previewMask.style.display = 'flex';
            preview.src = e.srcElement.src;
        }
    });

    with(preview) {
        style.width = '80%';
        style.margin = 'auto';
    }

    with(previewMask) {
        style.position = 'fixed';
        style.display = 'none';
        style.height = '100vh';
        style.width = '100vw';
        style.background = '#ccc8';
        appendChild(preview);
        addEventListener('click', function(){
            previewMask.style.display = 'none';
        });
    }

    with(loading) {
        style.display = 'inline-block';
        style.width = '100px';
        style.height = '100px';
        style.background = 'blue';
        style.margin = 'auto';
        className = 'loading';
    }

    with(loadingMask) {
        style.position = 'fixed';
        style.display = 'none';
        style.height = '100vh';
        style.width = '100vw';
        style.background = '#ccc8';
        appendChild(loading);
    }

    with(loadMore) {
        textContent = 'Load More';
        style.display = 'none';
        style.textAlign = 'center';
        style.background = 'lightBlue';
        style.lineHeight = '2.5';
        style.cursor = 'pointer';
        addEventListener('click', function(){
            search(input.value, true);
        });
    }

    with(input) {
        placeholder = 'input keywords';
        style.height = '25px';
        style.width = '95%';
        style.lineHeight = '2';
        style.fontSize = '20px';
        addEventListener('keyup', function(e){
            if(e.keyCode === 13) {
                if(input.value.length < 2) {
                    alert('Please enter at least 2 characters');
                } else {
                    search(input.value);
                }  
            }
        });
    }

    with(searchForm) {
        style.display = 'block';
        style.position = 'fixed';
        style.padding = '5px 0';
        style.width = '100%';
        style.textAlign = 'center';
        appendChild(input);
    }


    document.body.appendChild(loadingMask);
    document.body.appendChild(previewMask);
    document.body.appendChild(searchForm);
    document.body.appendChild(result);
    document.body.appendChild(loadMore);

    // init
    input.value = '女優';
    search(input.value);
});